﻿using System.Collections.Generic;
using System.Linq;
using QuotesServer.Interfaces;
using QuotesServer.Models;

namespace QuotesServer
{
	public class QuotesStorage : IQuotesStorage
	{
		private readonly Dictionary<string, Quote> quotes = new Dictionary<string, Quote>();
		private readonly object lockObject = new object();

		public List<Quote> GetAllQuotes()
		{
			lock (lockObject)
			{
				return quotes.Values.ToList();
			}
		}

		public List<Quote> GetNewQuotes()
		{
			lock (lockObject)
			{
				return quotes.Values.Where(x => x.IsNew).ToList();
			}
		}

		public void AddQuote(Quote quote)
		{
			lock (lockObject)
			{
				if (quotes.ContainsKey(quote.Symbol))
					quotes[quote.Symbol] = quote;
				else
					quotes.Add(quote.Symbol, quote);
			}
		}

		public void SetQuotesStatus(bool isNew)
		{
			lock (lockObject)
			{
				foreach (var quote in quotes.Values)
				{
					quote.IsNew = isNew;
				}
			}
		}
	}
}
