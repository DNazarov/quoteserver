﻿using System;
using QuotesServer.Models;

namespace QuotesServer.Interfaces
{
	public interface IQuotesSource
	{
		event EventHandler<Quote> NewQuote;
	}
}
