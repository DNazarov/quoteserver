﻿using System.Collections.Generic;
using QuotesServer.Models;

namespace QuotesServer.Interfaces
{
	public interface IQuotesStorage
	{
		List<Quote> GetAllQuotes();

		List<Quote> GetNewQuotes();

		void AddQuote(Quote quote);

		void SetQuotesStatus(bool isNew);
	}
}
