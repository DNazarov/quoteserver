﻿using System;
using System.Configuration;
using Common;

namespace QuotesServer
{
	public class Program
	{
		public static void Main(String[] args)
		{
			var server = new Server(Convert.ToInt32(ConfigurationManager.AppSettings["Port"]), new QuotesStorage(), new QuotesSource(), new TcpSocket());
			server.Start();
			Console.WriteLine("Server started");
			Console.ReadLine();
		}
	}
}
