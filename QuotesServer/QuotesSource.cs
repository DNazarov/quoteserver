﻿using System;
using System.Reactive.Linq;
using QuotesServer.Interfaces;
using QuotesServer.Models;

namespace QuotesServer
{
	public class QuotesSource : IQuotesSource
	{
		private const int interval = 600; //Interval in milliseconds
		private readonly Random random;

		private readonly string[] symbols = new[]
		{
			"RUB_EUR",
			"EUR_USD",
			"USD_CHG",
			"USD_AUD",
			"USD_GBP"
		};

		public event EventHandler<Quote> NewQuote;

		public QuotesSource()
		{
			random = new Random();

			Observable.Timer(TimeSpan.FromSeconds(1), TimeSpan.FromMilliseconds(interval))
					.Subscribe(timestamped => GenerateQuote());
		}

		private void GenerateQuote()
		{
			var quote = new Quote
			{
				Symbol = symbols[random.Next(0, symbols.Length - 1)],
				Rate = random.Next(10, 1000)/100m,
				IsNew = true
			};

			OnNewQuote(quote);
		}

		virtual protected void OnNewQuote(Quote quote)
		{
			var handler = NewQuote;

			if (handler != null)
				handler(this, quote);
		}
	}
}
