﻿using System.Collections.Generic;
using System.Text;
using QuotesServer.Models;

namespace QuotesServer.Helpers
{
	public static class QuotesFormatter
	{
		public static string GetMessage(List<Quote> quotes, string separator = ";")
		{
			var message = new StringBuilder();

			foreach (var quote in quotes)
			{
				message.AppendFormat("{0}={1}{2}", quote.Symbol, quote.Rate, separator);
			}

			var result = message.ToString();
			if (result.Length > 0 && result.Substring(result.Length - 1, 1) != "\n")
				result += "\n";

			return result;
		}
	}
}
