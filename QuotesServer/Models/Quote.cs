﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuotesServer.Models
{
	public class Quote
	{
		public string Symbol { get; set; }

		public decimal Rate { get; set; }

		public bool IsNew { get; set; }
	}
}
