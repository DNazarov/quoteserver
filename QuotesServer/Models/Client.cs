﻿using Common;

namespace QuotesServer.Models
{
	public class Client
	{
		public bool KeepProcessing { get; set; }
		public ISocket Socket { get; set; }
		public string Separator { get; set; }
	}
}
