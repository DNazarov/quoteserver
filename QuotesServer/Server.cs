﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using Common;
using QuotesServer.Helpers;
using QuotesServer.Interfaces;
using QuotesServer.Models;

namespace QuotesServer
{
	public class Server
	{
		#region Fields

		private readonly IQuotesStorage storage;
		private readonly IQuotesSource source;
		private readonly object lockObject = new object();

		private readonly int port;
		private readonly ISocket serverSocket;
		private readonly List<Client> clients = new List<Client>();
		private const int interval = 2; //Interval in seconds
		private bool runServer;

		#endregion

		#region Construction

		public Server(int port, IQuotesStorage storage, IQuotesSource source, ISocket serverSocket)
		{
			this.port = port;
			this.storage = storage;
			this.source = source;
			this.serverSocket = serverSocket;

			source.NewQuote += source_NewQuote;
		}

		~Server()
		{
			Stop();
		}

		#endregion

		#region Public methods

		public void Start()
		{
			var serverThread = new Thread(StartServer) { IsBackground = true };
			serverThread.Start();
		}

		public void Stop()
		{
			runServer = false;
			serverSocket.Close();
			lock (lockObject)
			{
				while (clients.Count > 0)
				{
					clients[0].KeepProcessing = false;
					clients[0].Socket.Close();
					clients.RemoveAt(0);
				}
			}
		}

		#endregion

		#region Private methods

		private void BindSocketAndListen()
		{
			serverSocket.Bind(new IPEndPoint(IPAddress.Any, port));
			serverSocket.Listen(10);
		}

		private void StartServer()
		{
			BindSocketAndListen();
			runServer = true;

			Observable.Timer(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(interval))
				.Subscribe(timestamped => SendQuotes());

			while (runServer)
			{
				var clientSocket = serverSocket.Accept();
				var client = new Client
				{
					KeepProcessing = false,
					Socket = clientSocket,
					Separator = ";"
				};
				lock (lockObject)
				{
					clients.Add(client);
				}

				var clientThread = new Thread(() => ProcessClient(client)) { IsBackground = true };
				clientThread.Start();
			}
		}

		private void ProcessClient(Client client)
		{
			try
			{
				var buff = new byte[1024];
				client.Socket.Send(Encoding.UTF8.GetBytes(QuotesFormatter.GetMessage(storage.GetAllQuotes())));

				lock (lockObject)
				{
					client.KeepProcessing = true;
				}

				while (client.KeepProcessing)
				{
					var bytesRec = client.Socket.Receive(buff);
					var command = Encoding.UTF8.GetString(buff, 0, bytesRec);

					ProcessCommand(command, client);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			finally
			{
				lock (lockObject)
				{
					client.Socket.Close();
					clients.Remove(client);
				}
			}
		}

		private void ProcessCommand(string command, Client client)
		{
			switch (command)
			{
				case "q":
					client.KeepProcessing = false;
					client.Socket.Send(Encoding.UTF8.GetBytes("Connection closed"));
					break;
				case "r":
					client.Separator = ";";
					break;
				case "c":
					client.Separator = "\n";
					break;
				default:
					client.Socket.Send(Encoding.UTF8.GetBytes("Unknown command"));
					break;
			}
		}

		private void SendQuotes()
		{
			var quotes = storage.GetNewQuotes();

			lock (lockObject)
			{
				var separators = clients.Select(x => x.Separator).Distinct().ToArray();
				var messages = separators.ToDictionary(s => s, s => QuotesFormatter.GetMessage(quotes, s));

				foreach (var client in clients)
				{
					try
					{
						if (client.KeepProcessing)
							client.Socket.Send(Encoding.UTF8.GetBytes(messages[client.Separator]));
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex.ToString());
					}
				}
			}

			storage.SetQuotesStatus(false);
		}

		private void source_NewQuote(object sender, Quote e)
		{
			storage.AddQuote(e);
		}

		#endregion
	}
}

