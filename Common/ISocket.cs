﻿using System.Net;

namespace Common
{
	public interface ISocket
	{
		ISocket Accept();
		void Connect(IPEndPoint ipEndPoint);
		void Close();
		int Receive(byte[] buffer);
		int Send(byte[] buffer);
		void Bind(EndPoint endPoint);
		void Listen(int backlog);
	}
}
