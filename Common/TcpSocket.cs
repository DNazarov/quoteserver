﻿using System.Net;
using System.Net.Sockets;

namespace Common
{
	public class TcpSocket : ISocket
	{
		private readonly Socket socket;

		public TcpSocket()
		{
			socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		}

		public TcpSocket(Socket socket)
		{
			this.socket = socket;
		}

		public ISocket Accept()
		{
			return new TcpSocket(socket.Accept());
		}

		public void Connect(IPEndPoint ipEndPoint)
		{
			socket.Connect(ipEndPoint);
		}

		public void Close()
		{
			socket.Close();
		}

		public int Receive(byte[] buffer)
		{
			return socket.Receive(buffer);
		}

		public int Send(byte[] buffer)
		{
			return socket.Send(buffer);
		}

		public void Bind(EndPoint endPoint)
		{
			socket.Bind(endPoint);
		}

		public void Listen(int backlog)
		{
			socket.Listen(backlog);
		}
	}
}
