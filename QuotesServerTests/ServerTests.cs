﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using Moq;
using NUnit.Framework;
using QuotesServer;
using QuotesServer.Helpers;
using QuotesServer.Interfaces;
using QuotesServer.Models;

namespace QuotesServerTests
{
	[TestFixture]
	public class ServerTests
	{
		private Mock<IQuotesStorage> storage;
		private Mock<IQuotesSource> source;
		private Mock<ISocket> serverSocket;
		private Mock<ISocket> clientSocket;
		private Server server;

		readonly List<Quote> allQuotes = new List<Quote>
		{
			new Quote { Symbol = "RUB_EUR", Rate = 1, IsNew = true },
			new Quote { Symbol = "USD_GBP", Rate = 1.5m, IsNew = false },
			new Quote { Symbol = "EUR_USD", Rate = 2.71m, IsNew = true },
			new Quote { Symbol = "USD_AUD", Rate = 0.3m, IsNew = false },
			new Quote { Symbol = "USD_AUD", Rate = 0.5m, IsNew = true }
		};

		[SetUp]
		public void Init()
		{
			storage = new Mock<IQuotesStorage>();
			source = new Mock<IQuotesSource>();
			serverSocket = new Mock<ISocket>();
			clientSocket = new Mock<ISocket>();

			serverSocket.Setup(ss => ss.Accept())
				.Returns(clientSocket.Object);

			server = new Server(3333, storage.Object, source.Object, serverSocket.Object);
		}

		[Test]
		public void SourceEventTest()
		{
			storage.Setup(st => st.AddQuote(It.IsAny<Quote>()));

			source.Raise(so => so.NewQuote += null, this, new Quote());

			storage.VerifyAll();
		}

		[Test]
		public void InitialMessageTest()
		{
			var result = false;

			storage.Setup(st => st.GetAllQuotes())
				.Returns(allQuotes);

			clientSocket.Setup(cs => cs.Send(It.IsAny<byte[]>()))
				.Callback((byte[] buff) => result = CompareBuffers(buff, Encoding.UTF8.GetBytes(QuotesFormatter.GetMessage(allQuotes))) || result);

			server.Start();

			Thread.Sleep(1000);

			clientSocket.VerifyAll();
			Assert.IsTrue(result);
		}

		[Test]
		public void ColunmCommandTest()
		{
			var result = false;
			var command = "c";

			storage.Setup(st => st.GetAllQuotes())
				.Returns(allQuotes);
			storage.Setup(st => st.GetNewQuotes())
				.Returns(allQuotes.Where(x => x.IsNew).ToList());

			clientSocket.Setup(cs => cs.Send(It.IsAny<byte[]>()))
				.Callback((byte[] buff) => result = CompareBuffers(buff, Encoding.UTF8.GetBytes(QuotesFormatter.GetMessage(allQuotes.Where(x => x.IsNew).ToList(), "\n"))) || result);

			clientSocket.Setup(cs => cs.Receive(It.IsAny<byte[]>()))
				.Returns((byte[] buffer) => CopyBuffer(buffer, command));

			server.Start();

			clientSocket.Object.Receive(Encoding.UTF8.GetBytes(command));

			Thread.Sleep(4000);

			clientSocket.VerifyAll();
			Assert.IsTrue(result);
		}

		[Test]
		public void QuitCommandTest()
		{
			var result = false;
			var command = "q";

			storage.Setup(st => st.GetAllQuotes())
				.Returns(allQuotes);

			clientSocket.Setup(cs => cs.Send(It.IsAny<byte[]>()))
				.Callback((byte[] buff) => result = CompareBuffers(buff, Encoding.UTF8.GetBytes("Connection closed")) || result);

			clientSocket.Setup(cs => cs.Receive(It.IsAny<byte[]>()))
				.Returns((byte[] buffer) => CopyBuffer(buffer, command));

			server.Start();

			clientSocket.Object.Receive(Encoding.UTF8.GetBytes(command));

			Thread.Sleep(3000);

			clientSocket.VerifyAll();
			Assert.IsTrue(result);
		}

		[Test]
		public void SheduleQuotesTest()
		{
			var result = false;

			storage.Setup(st => st.GetAllQuotes())
				.Returns(allQuotes);
			storage.Setup(st => st.GetNewQuotes())
				.Returns(allQuotes.Where(x => x.IsNew).ToList());

			clientSocket.Setup(cs => cs.Send(It.IsAny<byte[]>()))
				.Callback((byte[] buff) => result = CompareBuffers(buff, Encoding.UTF8.GetBytes(QuotesFormatter.GetMessage(allQuotes.Where(x => x.IsNew).ToList()))) || result);

			server.Start();

			Thread.Sleep(3000);

			clientSocket.VerifyAll();
			Assert.IsTrue(result);
		}

		#region Private methods
		
		private bool CompareBuffers(byte[] buffer1, byte[] buffer2)
		{
			if (buffer1.Length != buffer2.Length) return false;
			for (int i = 0; i < buffer1.Length; i++)
			{
				if (buffer1[i] != buffer2[i]) return false;
			}
			return true;
		}

		private int CopyBuffer(byte[] buffer, string command)
		{
			byte[] str = Encoding.UTF8.GetBytes(command);
			for (int i = 0; i < str.Length; i++)
				buffer[i] = str[i];
			return str.Length;
		}

		#endregion

	}
}
