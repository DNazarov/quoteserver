﻿using NUnit.Framework;
using QuotesServer;
using QuotesServer.Models;

namespace QuotesServerTests
{
	[TestFixture]
	public class StorageTests
	{
		private QuotesStorage storage;

		readonly Quote quote1 = new Quote { Symbol = "RUB_EUR", Rate = 1, IsNew = true };
		readonly Quote quote2 = new Quote { Symbol = "USD_GBP", Rate = 1.5m, IsNew = false };
		readonly Quote quote3 = new Quote { Symbol = "EUR_USD", Rate = 2.71m, IsNew = true };
		readonly Quote quote4 = new Quote { Symbol = "USD_AUD", Rate = 0.3m, IsNew = false };
		readonly Quote quote5 = new Quote { Symbol = "USD_AUD", Rate = 0.5m, IsNew = true };

		[SetUp]
		public void Init()
		{
			storage = new QuotesStorage();
		}

		[Test]
		public void AddQuotesTest()
		{
			storage.AddQuote(quote1);
			storage.AddQuote(quote1);
			storage.AddQuote(quote4);
			storage.AddQuote(quote5);

			Assert.AreEqual(2, storage.GetAllQuotes().Count);
		}

		[Test]
		public void GetNewQuotesTest()
		{
			storage.AddQuote(quote1);
			storage.AddQuote(quote2);
			storage.AddQuote(quote4);

			Assert.AreEqual(1, storage.GetNewQuotes().Count);
		}

		[Test]
		public void SetQuotesStatusTest()
		{
			storage.AddQuote(quote1);
			storage.AddQuote(quote2);
			storage.AddQuote(quote3);
			storage.AddQuote(quote4);

			storage.SetQuotesStatus(false);

			Assert.AreEqual(0, storage.GetNewQuotes().Count);
		}
	}
}
