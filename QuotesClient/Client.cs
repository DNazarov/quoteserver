﻿using System;
using System.Net;
using System.Text;
using System.Threading;
using Common;

namespace QuotesClient
{
	public class Client
	{
		private readonly string ip;
		private readonly int port;
		private readonly ISocket clientSocket;

		private bool runClient;

		public Client(string ip, int port)
		{
			this.ip = ip;
			this.port = port;

			clientSocket = new TcpSocket();
		}

		~Client()
		{
			Stop();
		}

		public void Start()
		{
			try
			{
				var ipAddr = IPAddress.Parse(ip);
				var ipEndPoint = new IPEndPoint(ipAddr, port);

				clientSocket.Connect(ipEndPoint);
				runClient = true;

				var quotesThread = new Thread(GetQuotes) { IsBackground = true };
				quotesThread.Start();

				ProcessCommands();
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error during initialization: " + ex.Message);
			}
		}

		public void Stop()
		{
			runClient = false;
			clientSocket.Close();
		}

		private void GetQuotes()
		{
			try
			{
				var buff = new byte[1024];

				while (runClient)
				{
					var bytesRec = clientSocket.Receive(buff);
					var message = Encoding.UTF8.GetString(buff, 0, bytesRec);
					if (message != string.Empty)
					{
						Console.WriteLine(message);
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		private void ProcessCommands()
		{
			try
			{
				while (runClient)
				{
					var message = Console.ReadLine();
					var msg = Encoding.UTF8.GetBytes(message);
					clientSocket.Send(msg);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
