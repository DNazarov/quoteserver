﻿using System;
using System.Configuration;

namespace QuotesClient
{
	class Program
	{
		static void Main(string[] args)
		{
			var client = new Client(ConfigurationManager.AppSettings["Ip"],
				Convert.ToInt32(ConfigurationManager.AppSettings["Port"]));

			client.Start();

			Console.ReadLine();
		}
	}
}
